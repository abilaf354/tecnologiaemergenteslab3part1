package com.nestor.laboratorio3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide



class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val personasList = mutableListOf<Personas>(
            Personas("JOSE", "MORALES", "2313526"),

        )
        val listView = findViewById<ListView>(R.id.listView)
        val personasAdapter = PersonasAdapter(this, personasList)
        listView.adapter = personasAdapter
        // Botón para agregar una nueva persona
        val btnAgregar = findViewById<Button>(R.id.btnAgregar)
        btnAgregar.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Agregar Persona")

            val view = layoutInflater.inflate(R.layout.registrar, null)
            val etNombre = view.findViewById<EditText>(R.id.etNombre)
            val etApellido = view.findViewById<EditText>(R.id.etApellido)
            val etTelefono = view.findViewById<EditText>(R.id.etTelefono)

            builder.setView(view)

            builder.setPositiveButton("Agregar") { dialog, _ ->
                val nombre = etNombre.text.toString()
                val apellido = etApellido.text.toString()
                val telefono = etTelefono.text.toString()

                // Agregar lógica para agregar la persona a la lista
                val nuevaPersona = Personas(nombre, apellido, telefono)
                personasList.add(nuevaPersona)
                personasAdapter.notifyDataSetChanged()

                dialog.dismiss()
            }

            builder.setNegativeButton("Cancelar") { dialog, _ ->
                dialog.dismiss()
            }

            builder.show()
        }


        // Botón para modificar una persona existente
        // Botón para modificar una persona existente
        val btnModificar = findViewById<Button>(R.id.btnModificar)
        btnModificar.setOnClickListener {
            // Obtener la persona que se va a modificar (por ejemplo, la primera persona en la lista)
            val personaAModificar = personasList[0] // Aquí necesitas implementar la lógica para obtener la persona adecuada

            // Crear el cuadro de diálogo de modificar
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Modificar Persona")

            // Inflar el diseño XML para el cuadro de diálogo de modificar
            val view = layoutInflater.inflate(R.layout.modificar, null)

            // Obtener las referencias a los EditText en el diseño del cuadro de diálogo
            val etNuevoNombre = view.findViewById<EditText>(R.id.etNuevoNombre)
            val etNuevoApellido = view.findViewById<EditText>(R.id.etNuevoApellido)
            val etNuevoTelefono = view.findViewById<EditText>(R.id.etNuevoTelefono)

            // Establecer los datos actuales de la persona en los EditText
            etNuevoNombre.setText(personaAModificar.nombre)
            etNuevoApellido.setText(personaAModificar.apellido)
            etNuevoTelefono.setText(personaAModificar.telefono)

            // Establecer la vista personalizada en el cuadro de diálogo
            builder.setView(view)

            // Configurar el botón "Modificar" para actualizar los datos de la persona
            builder.setPositiveButton("Modificar") { dialog, _ ->
                // Obtener los nuevos datos ingresados por el usuario
                val nuevoNombre = etNuevoNombre.text.toString()
                val nuevoApellido = etNuevoApellido.text.toString()
                val nuevoTelefono = etNuevoTelefono.text.toString()

                // Actualizar los datos de la persona en la lista
                personaAModificar.nombre = nuevoNombre
                personaAModificar.apellido = nuevoApellido
                personaAModificar.telefono = nuevoTelefono

                // Notificar al adaptador que los datos han cambiado
                personasAdapter.notifyDataSetChanged()

                // Cerrar el cuadro de diálogo
                dialog.dismiss()
            }

            // Configurar el botón "Cancelar" para cerrar el cuadro de diálogo sin realizar cambios
            builder.setNegativeButton("Cancelar") { dialog, _ ->
                dialog.dismiss()
            }

            // Mostrar el cuadro de diálogo
            builder.show()
        }



        // Botón para eliminar una persona existente
        val btnEliminar = findViewById<Button>(R.id.btnEliminar)
        btnEliminar.setOnClickListener {
            val posicion = 0 // Indica la posición de la persona que deseas eliminar
            personasList.removeAt(posicion)
            personasAdapter.notifyDataSetChanged()

            // Aquí implementa la lógica para eliminar una persona existente
            // Elimina la persona de la lista y notifica al adaptador
        }

    }

    }